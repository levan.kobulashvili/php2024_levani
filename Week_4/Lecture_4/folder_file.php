<?php
    $dir_error = "";
    $file_error = "";

    if(isset($_POST['c_folder']) && !empty($_POST['c_folder']))
    {
        $f_url = "root/".$_POST['c_folder'];

        if(is_dir($f_url))
        {
            $dir_error = "Folder already exists";
        } else {
            mkdir($f_url);
        }
    }

    if(isset($_POST['c_file']) && !empty($_POST['c_file'])) {
        $f_url = "root/".$_POST['c_file'].".txt";
    
        if (file_exists($f_url)) {
            $file_error = "File already exists";
        } else {
            fopen($f_url, 'w');
        }
    }




?>