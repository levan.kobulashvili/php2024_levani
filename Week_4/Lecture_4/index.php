<?php
    include "folder_file.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lecture 4</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        <form method="post" action="">
            <div>
                <input type="text" name="c_folder" placeholder="Create Folder">
                <button>Create Folder</button> <span><?=$dir_error?></span>
            </div>
            <div>
                <input type="text" name="c_file" placeholder="Create File">
                <button>Create File</button> <span><?=$file_error?></span>
            </div>
        </form>
    </header>
    <main>
        <?php
            $directory = 'root';
            $files = scandir($directory);
            
            foreach ($files as $file)
            {
                if ($file != '.' && $file != '..')
                    echo $file. "<br>";
            }
        ?>
    </main>
</body>
</html>