<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lab 1 task 2</title>
</head>
<body>
    <form action="task2table.php" method="get">
        <span>Student name: </span>
            <input type="text" name="sfn">
            <br><br>
        <span>Student surname: </span>
            <input type="text" name="sln">
            <br><br>
        <span>semester: </span>
            <select name="semester">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
            </select>
            <br><br>

        <span>Grade: </span>
            <input type="number" name="grade" min="0" max="100">
            <br><br>   

        <span>Lecturer name: </span>
            <input type="text" name="lfn">
            <br><br>
        <span>Lecturer surname: </span>
            <input type="text" name="lln">
            <hr>
        <button>CHECK</button>
    </form>
</body>
</html>
