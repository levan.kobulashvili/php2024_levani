<?php
    $sfn = $_GET['sfn'];
    $sln = $_GET['sln'];
    $semester = $_GET['semester'];
    $grade = $_GET['grade'];
    $gradeLetter="";
    $lfn = $_GET['lfn'];
    $lln = $_GET['lln'];

   
    switch ($grade) {
        case $grade>=91:
            $gradeLetter="A";
            break;
        case $grade>=81:
            $gradeLetter="B";
            break;
        case $grade>=71:
            $gradeLetter="C";
            break;
        case $grade>=61:
            $gradeLetter="D";
            break;
        case $grade>=51:
            $gradeLetter="E";
            break;
        case $grade>=41:
            $gradeLetter="F";
            break;
        default:
            $gradeLetter="FX";
        ;
    }
    $table= "
        <table border='1' style='border-collapse: collapse;'>
        <thead>
            <td>Student Name</td>
            <td>Student Surname</td>
            <td>Semester</td>
            <td>Grade</td>
            <td>Mark</td>
            <td>Lecturer Name</td>
            <td>Lecturer Surname</td>
        </thead>
        <tbody>
            <td>".$sfn."</td>
            <td>".$sln."</td>
            <td>".$semester."</td>
            <td>".$grade."</td>
            <td>".$gradeLetter."</td>
            <td>".$lfn."</td>
            <td>".$lln."</td>
        </tbody>
    </table>";

    echo  $table
?>
