<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lecture 1</title>
</head>
<body>
    <h1>Hello World!!!</h1>
    <form action="get.php" method="get">
        <input placeholder="get" type="text" name="saxeli">
        <br><br>
        <button>SEND</button>
    </form>

    <hr><hr>

    <form action="post.php" method="post">
        <input placeholder="post" type="text" name="saxeli">
        <br><br>
        <button>SEND</button>
    </form>

    <?php
        echo "<h1>Hello PHP</h1>";

        $x = 17;
        $s = "GAU";
        echo $x;
        echo "<h2>$s</h2>";

        $a = [12, "GAU", 23, FALSE];
        echo "<hr>";
        print_r($a);

        $as =
        [ 
            "Name" => "Levan",
            "Age" => 19,
            "GPA" => 3.83
        ];
        
        echo "<hr>";
        print_r($as);


    ?>
</body>
</html>