<?php
    include "questions.php";
    // shuffle($questions);
    // echo "<pre>";
    // print_r($questions);
    // echo "</pre>";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Quiz</title>
</head>
<body>
    <div class="container">
        <form action="lecturer.php" method="post">
            <h2>Welcome to the Quiz!</h2>

            <div class="row title">
                <div>Questions</div>
                <div>Points</div>
                <div>Answers</div>
            </div>

            <?php foreach ($questions as $item): ?>
                <div class="row">
                    <div class = "question"><?=$item['question']?></div>
                    <div class = "point"><?=$item['point']?></div>
                    <div><input type="text" placeholder="Answer" name="answer[]"></div>
                </div>
            <?php endforeach; ?>

            <div class="row">
                <label>Student:</label>
                <input type="text" placeholder="First Name" name="firstName">
                <input type="text" placeholder="Last Name" name="lastName">
                <button>SUBMIT</button>
            </div>
        </form>
    </div>
</body>
</html>