<?php
    include "questions.php";
    // shuffle($questions);


     echo "<pre>";
     print_r($_POST);
     echo "</pre>";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Grade</title>
</head>
<body>
    <div class="container">
        <form action="lecturer.php" method="post">
            <h2>Grading - <?=$_POST['firstName']." ".$_POST['lastName']?></h2>


            <div class="row title">
                <div class = "question">Questions</div>
                <div class = "point">Points</div>
                <div class = "answer">Answers</div>
                <div>Grading</div>
            </div>

            <?php
                $i = 0;
                foreach ($questions as $item): ?>
                <div class="row">
                    <div class = "question"><?=$item['question']?></div>
                    <div class = "point"><?=$item['point']?></div>
                    <div class="answer"><?=$_POST['answer'][$i]?></div>
                    <div><input type="text" placeholder="Grade" size="10"></div>
                </div>
            <?php $i++; endforeach; ?>

            <div class="row">
                <label>Lecturer:</label>
                <input type="text" placeholder="First Name">
                <input type="text" placeholder="Last Name">
                <button>SUBMIT</button>
            </div>
        </form>
    </div>
</body>
</html>